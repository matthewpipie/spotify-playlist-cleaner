#!/bin/env python
import json
import os

thisDir = os.path.dirname(os.path.abspath(__file__))

jobsDir = os.path.join(thisDir, 'jobs')

IGNORE_LIST_FILE = os.path.join(thisDir, 'ignoreList.json')

def writeIgnoreList(data):
    with open(IGNORE_LIST_FILE,'w') as filey:
        return json.dump(data, filey)

def loadIgnoreList():
    try:
        with open(IGNORE_LIST_FILE,'r') as filey:
            return json.load(filey)
    except IOError:
        pass
    return []

ignoreList = loadIgnoreList()

def loadId(iD):
    try:
        with open(os.path.join(jobsDir, iD), 'r') as filey:
            return json.load(filey)
    except IOError:
        pass
    return {}

def writeId(iD, data):
    with open(os.path.join(jobsDir, iD), 'w') as filey:
        return json.dump(data, filey)

filesTodo = [f for f in os.listdir(jobsDir) if f not in ignoreList]

def callback(iD, info):
    writeId(iD, info)
    return info

def getIdFromInput(string):
    if len(string.split(':')) == 5:
        return string.split(':')[4]
    elif 'open.spotify.com' in string:
        if len(string.split('/')) == 7:
            return string.split('/')[6]
    return None

for filey in filesTodo:
    try:
        if filey in ignoreList:
            continue
        obj = loadId(filey)
        if (obj['progress']['clean-uri'] is not None and len(obj['progress']['clean-uri']) > 0 ) or obj['progress']['failed']:
            ignoreList.append(filey)
            continue

        import cleaner
        import StringIO
        successes = StringIO.StringIO()
        failures = StringIO.StringIO()

        cl = cleaner.Cleaner(filey, getIdFromInput(obj['uri']), successes, failures, callback)

        writeId(filey, cl.jobInfo)

        successes.close()
        failures.close()
    except Exception as e:
        print "ripye"
        print filey
        print e
        writeId(filey, {'uri': '', 'token': '', 'progress': {'successes': [], 'failures': [], 'failed': True, 'clean-uri': None}})
    ignoreList.append(filey)

writeIgnoreList(ignoreList)

