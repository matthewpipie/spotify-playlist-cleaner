import sys
import json
import os
import hashlib
import time

try:
    uri = sys.argv[1]
    token = sys.argv[2]
    jobId = hashlib.sha224(uri + token + str(time.time())).hexdigest()

    thisDir = os.path.dirname(os.path.abspath(__file__))

    jobsDir = os.path.join(thisDir, 'jobs')
    #os.makedirs(jobsDir)

    fileName = os.path.join(jobsDir, jobId)

    baseObj = {}
    baseObj['progress'] = {}
    baseObj['progress']['clean-uri'] = None
    baseObj['progress']['failed'] = False
    baseObj['progress']['successes'] = []
    baseObj['progress']['failures'] = []
    baseObj['uri'] = uri
    baseObj['token'] = token

    with open(fileName,'w') as filey:
        json.dump(baseObj, filey)

    print jobId
except Exception:
    print "error"
