#!/bin/python2
import os
import time
import json

MAX_ALBUM_ITERATIONS=10

class sp_wrapper:
    def __init__(self, token):
        import spotipy
        self.sp = spotipy.Spotify(auth=token)
        self.cache = {}
        self.dispatch = {
                'user_playlist': self.R_user_playlist,
                'next': self.R_next,
                'user_playlist_create': self.R_user_playlist_create,
                'search': self.R_search,
                'current_user_playlists': self.R_current_user_playlists,
                'album_tracks': self.R_album_tracks,
                'user_playlist_add_tracks': self.R_user_playlist_add_tracks,
                'track': self.R_track,
                'me': self.R_me,
                }
        for key in self.dispatch:
            self.cache[key] = {}


    def user_playlist(self, user, playlist_id, fields):
        return self.process('user_playlist', user, playlist_id, fields)
    def next(self, thing):
        return self.process('next', thing)
    def user_playlist_create(self, user,name,public,description):
        return self.process('user_playlist_create',  user,name,public,description)
    def search(self, q, type):
        return self.process('search',  q, type)
    def current_user_playlists(self, limit, offset):
        return self.process('current_user_playlists',  limit, offset)
    def album_tracks(self, iD):
        return self.process('album_tracks',  iD)
    def user_playlist_add_tracks(self, user,iD,tracks):
        return self.process('user_playlist_add_tracks', user, iD, tracks)
    def track(self, track):
        return self.process('track', track)
    def me(self):
        return self.process('me')
    def process(self, func, *args):
        from requests.exceptions import ConnectionError
        hashCode = str(args)
        if hash(hashCode) in self.cache[func]:
            return self.cache[func][hash(hashCode)]
        res = None
        while True:
            try:
                res = self.dispatch[func](*args)
                break
            except ConnectionError:
                time.sleep(1)
                continue
        self.cache[func][hash(hashCode)] = res
        return res


    def R_user_playlist(self, user, playlist_id, fields):
        return self.sp.user_playlist(user, playlist_id=playlist_id, fields=fields)
    def R_next(self, thing):
        return self.sp.next(thing)
    def R_user_playlist_create(self, user,name,public,description):
        return self.sp.user_playlist_create(user,name,public=public),#description=description)
    def R_search(self, q, type):
        return self.sp.search(q=q,type=type)
    def R_current_user_playlists(self, limit, offset):
        return self.sp.current_user_playlists(limit=limit, offset=offset)
    def R_album_tracks(self, iD):
        return self.sp.album_tracks(iD)
    def R_user_playlist_add_tracks(self, user,iD,tracks):
        return self.sp.user_playlist_add_tracks(user, iD, tracks)
    def R_track(self, track):
        return self.sp.track(track)
    def R_me(self):
        return self.sp.me()

thisDir = os.path.dirname(os.path.abspath(__file__))

ALBUM_CACHE_FILE = os.path.join(thisDir, 'albumCache.json')
SONG_CACHE_FILE = os.path.join(thisDir, 'songCache.json')
JOBS_DIR = os.path.join(thisDir, 'jobs')

def loadAlbumCache():
    try:
        with open(ALBUM_CACHE_FILE,'r') as filey:
            return json.load(filey)
    except IOError:
        pass
    return {}


def loadSongCache():
    try:
        with open(SONG_CACHE_FILE,'r') as filey:
            return json.load(filey)
    except IOError:
        pass
    return {}


def writeAlbumCache(data):
    with open(ALBUM_CACHE_FILE,'w') as filey:
        json.dump(data, filey)

def writeSongCache(data):
    with open(SONG_CACHE_FILE,'w') as filey:
        json.dump(data, filey)

def openJobId(iD):
    with open(os.path.join(JOBS_DIR,iD),'r') as filey:
        return json.load(filey)

class Cleaner:
    def __init__(self, jobId, playlistID, successesStream, failuresStream, callbackOnEachFinish):
        self.jobId = jobId
        self.jobInfo = openJobId(jobId)
        self.spw = sp_wrapper(self.jobInfo['token'])
        self.albumCache = loadAlbumCache()
        self.songCache = loadSongCache()
        self.successesStream = successesStream
        self.failuresStream = failuresStream
        self.callbackOnEachFinish = callbackOnEachFinish
        self.user = self.spw.me()['id']
        #try:
        if playlistID is None:
            raise Exception
        self.cleanURI = self.mainer(playlistID)
        #except Exception as e:
            #print "EXCEPTION!"
            #print e
            #self.cleanURI = None
	writeAlbumCache(self.albumCache)
        writeSongCache(self.songCache)
        self.jobInfo['progress']['clean-uri'] = self.cleanURI
        if self.cleanURI is None:
            self.jobInfo['progress']['failed'] = True

    def mainer(self, playlistID):
        if playlistID is None:
            playlistID = self.getPlaylistSelectionID()
        playlist = self.spw.user_playlist(self.user, playlist_id=playlistID, fields="tracks,next,name,public")
        tempTracks = playlist['tracks']
        tracks = []
        tracks.extend(tempTracks['items'])
        while tempTracks['next']:
            tempTracks = self.spw.next(tempTracks)
            tracks.extend(tempTracks['items'])
        cleanTracks = []
        for index in range(0,len(tracks)):
            temp = self.getCleanTrack(tracks[index])
            if temp is not None:
                cleanTracks.append(temp['id'])
                print >>self.successesStream,tracks[index]['track']['name'] + " (" + self.getArtistsNames(tracks[index]['track']) + ")"
            else:
                print >>self.failuresStream,tracks[index]['track']['name'] + " (" + self.getArtistsNames(tracks[index]['track']) + ")"

            self.jobInfo['progress']['successes'] = self.successesStream.getvalue().split('\n')
            self.jobInfo['progress']['failures'] = self.failuresStream.getvalue().split('\n')
            self.callbackOnEachFinish(self.jobId, self.jobInfo)

        return self.makePlaylistFromURIs(cleanTracks, playlist)

    def makePlaylistFromURIs(self, trackURIs, oldPlaylist):
        newURI = self.spw.user_playlist_create(self.user, oldPlaylist['name'] + " (clean)", public=oldPlaylist['public'], description="Cleaned by matthewpipie\'s Spotify Cleaner")[0]['uri']
        newID = newURI.split(":")[4]
        
        
# you can only add tracks in lists with lengths of 100
        trackURI100s = [trackURIs[i:i+100] for i in range(0,len(trackURIs),100)]
        for index in range(0,len(trackURI100s)):
            self.spw.user_playlist_add_tracks(self.user,newID,trackURI100s[index])
        return newURI

    def getArtistsNames(self, track):
        artistsNames = ""
        for artist in track['artists']:
            artistsNames += artist['name'] + ", "
        return artistsNames[:-2]

        
    def getCleanTrack(self, track):
        if not track['track']['explicit']:
            return track['track']
        if track['track']['id'] in self.songCache:
            return self.spw.track(self.songCache[track['track']['id']])
        cleanAlbumID = None
        counter = 1
        if track['track']['album']['id'] not in self.albumCache:
            tempSerch = self.spw.search(q='album:\"' + track['track']['album']['name'] + '\"', type='album')['albums']
            for index in range(0,len(tempSerch['items'])):
                if self.isCleanAlbum(track['track'], tempSerch['items'][index]):
                    cleanAlbumID = tempSerch['items'][index]['id']
                    break
            while tempSerch['next'] and cleanAlbumID is None and counter != MAX_ALBUM_ITERATIONS:
                counter += 1
                tempSerch = self.spw.next(tempSerch)['albums']
                for index in range(0,len(tempSerch['items'])):
                    if self.isCleanAlbum(track['track'], tempSerch['items'][index]):
                        cleanAlbumID = tempSerch['items'][index]['id']
                        break
 
            if cleanAlbumID is not None and self.allSongsAreClean(cleanAlbumID):
                self.albumCache[track['track']['album']['id']] = cleanAlbumID
        else:
            cleanAlbumID = self.albumCache[track['track']['album']['id']]
        if cleanAlbumID is None:
            return None
        ret = self.getTrackInAlbum(track['track']['disc_number'], track['track']['track_number'], cleanAlbumID)
        if ret is not None:
            self.songCache[track['track']['id']] = ret['id']
        return ret

    def isCleanAlbum(self, dirtyTrack, cleanAlbum):
        dirtyAlbum = dirtyTrack['album']
        propsEqual = ['name']
        propsNotEqual = ['id']
        for x in propsEqual:
            if dirtyAlbum[x] != cleanAlbum[x]:
                return False
        for x in propsNotEqual:
            if dirtyAlbum[x] == cleanAlbum[x]:
                return False
        if self.getArtistsNames(dirtyAlbum) != self.getArtistsNames(cleanAlbum):
            return False
        t = self.getTrackInAlbum(dirtyTrack['disc_number'], dirtyTrack['track_number'], cleanAlbum['id'])
        if t is None:
            return False
        if t['explicit']:
            return False
        return True

    def allSongsAreClean(self,cleanAlbumID):
        tempTracks = self.spw.album_tracks(cleanAlbumID)
        tracks = []
        tracks.extend(tempTracks['items'])
        while tempTracks['next']:
            tempTracks = self.spw.next(tempTracks)
            tracks.extend(tempTracks['items'])
        for index in range(0,len(tracks)):
            if tracks[index]['explicit']:
                return False
        return True


    def getTrackInAlbum(self, discNumber, trackNumber, albumID):
        tempTracks = self.spw.album_tracks(albumID)
        tracks = []
        tracks.extend(tempTracks['items'])
        while tempTracks['next']:
            tempTracks = self.spw.next(tempTracks)
            tracks.extend(tempTracks['items'])
        for index in range(0,len(tracks)):
            if tracks[index]['disc_number'] == discNumber and tracks[index]['track_number'] == trackNumber:
                return tracks[index]


    def getPlaylistSelectionID(self):
        return self.getPlaylistSelectionURI().split(':')[4]
    def getPlaylistSelectionURI(self):
        a = 0
        while a != 1 and a != 2:
            a = int(raw_input("Type 1 to select one of your own playlists or 2 to provide a spotify uri:"))
        if (a == 1):
            retList = self.getAllPlaylists()
            for index in range(0,len(retList)):
                print str(index+1) + ": " + retList[index]['name']
            b = 0
            while b < 1 or b > len(retList):
                b = int(raw_input("Make a selection (1-" + str(len(retList))+ "): "))
            return retList[b-1]['uri']
        else:
            c = ""
            while c == "":
                c = raw_input("Input the uri: ")
            return c

    def getAllPlaylists(self):
        off = 0
        lim = 50
        res = []
        nextThing = 1
        while nextThing != None:
            out = self.spw.current_user_playlists(limit=lim, offset=off)
            res.extend(out['items'])
            nextThing = out['next']
            off += lim
        return res

if (__name__=="__main__"):
    import sys
    Cleaner(sys.argv[1], None, None, None, lambda: None) # Token

